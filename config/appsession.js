var config = require ('../config/connection_config.js');

var Session = {
    secret: config.secret,
    session: null
};

module.exports = Session;