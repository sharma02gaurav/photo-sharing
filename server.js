var express = require('express');
var parser = require('body-parser');
var morgan = require('morgan');
var server = express();
var baseRoute = require('./routes/base_router.js');
var userRoute = require('./routes/user_router.js');
var authRoute = require('./routes/auth_router.js');
var postRoute = require ('./routes/post_router.js');
var jwt = require('jsonwebtoken');
var config = require('./config/connection_config.js');
server.set('secret', config.secret);
server.use(parser.json());
server.use(morgan('dev'));
// application routes

server.use('/', baseRoute);
server.use('/api', userRoute);
server.use('/api', authRoute);
server.use ('/api', postRoute);

// resources route
server.use('/res', express.static('resources'));
server.listen(3000);
console.log('listening on port 3000');
