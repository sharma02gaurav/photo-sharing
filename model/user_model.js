/*
    the model module for the user
*/
var mongoose = require ('mongoose');
var Schema = mongoose.Schema;

var config = require ('../config/connection_config.js');
var host = config.host;
var db = config.db;

var Comment = new Schema ({
    username: Object,
    comment: String,
    time: Date
});

var Picture = new Schema ({
    data: Buffer,
    contentType: String
});

var Post = new Schema ({
    post: String,
    image: Picture,
    comments: [Comment],
    time: Date
});

var Followers = new Schema({
    username: Object
});

var Following = new Schema({
    username: Object
});

var User = new Schema ({
    name: String,
    username: String,
    displayPic: Picture,
    password: String,
    posts: [Post],
    followers: [Followers],
    following: [Following]
});

var UserModel = mongoose.model ('users', User);

module.exports = UserModel;
