/**
 * the model for pic
 */

var mongoose = require ('mongoose');
var Schema = mongoose.Schema;

var config = require ('../config/connection_config.js');
var host = config.host;
var db = config.db;

var Picture = new Schema({
    data: Buffer,
    contentType: String
});

var UserPictures = new Schema ({
    username: String,
    pictures: [Picture]
});

var PictureModel = mongoose.model ('pictures', UserPictures);

module.exports = PictureModel;