/**
 * this is the authentication model 
 */

var mongoose = require ('mongoose');
var config = require ('../config/connection_config.js');
var Schema = mongoose.Schema;

var Auth = new Schema ({
    username: String,
    password: String
});

var AuthModel = mongoose.model ('auths', Auth);

module.exports = AuthModel;