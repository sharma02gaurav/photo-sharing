/*
    contains basic default routing
*/
var express = require ('express');
var router = express.Router();
var path = require ('path');
//var pug = require ('pug');

var tokenProperty = require ('../config/token.js');

router.use (function (req, res, next){
    next();
});

router.get ('/resources/*', function (req, res, next) {
    res.status (400);
    res.type ('html');
    res.sendFile (path.join(__dirname+ '/../views/errors/unauthorized.html'));
});

router.get ('/views/errors/*', function (req, res, next) {
    res.status (400);
    res.type ('html');
    res.sendFile (path.join(__dirname+ '/../views/errors/unauthorized.html'));
});
router.get ('/home', function (req, res, next) {
    // render the home app
    res.status (200);
    res.type ('html');
    res.sendFile (path.join(__dirname+ '/../views/index.html'));
});
router.get ('/login', function (req, res, next) {
    res.status (200);
    res.type ('html');
    res.sendFile (path.join(__dirname+ '/../views/login.html'));
});
router.get ('/account', function (req, res, next) {
    res.status (200);
    res.type ('html');

    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    if (token) {
        tokenProperty.token.verify (token, tokenProperty.secret, function (err, decoded) {
            if (err) {
                res.type ('json');
                //res.send ({status:'error', message: 'failed to authenticate token'});
                res.redirect ('/login');
            } else {
                req.decoded = decoded;

                //console.log (decoded);
                res.sendFile (path.join(__dirname+ '/../views/account.html'));
            }
        });
    }

    res.sendFile (path.join (__dirname+ '/../views/account.html'));
});

router.get ('/account/:uname', function (req, res, next) {
    res.status ();
});

module.exports = router;
