/**
 * this router handles the posts related routing
 */
var express = require ('express');
var router = express.Router();
var mongoose = require ('mongoose');
var tokenProperty = require ('../config/token.js');

var config = require ('../config/connection_config.js');
var UserSchema = require ('../model/user_model.js');

// get a specific post
router.get ('/post/:username/:postId', function (req, res, next) {
    var username = req.params.username;
    var postId = req.params.postId;

    res.type ('json');

    if (username == null || postId == null) {
        res.send ({status:'error', message: 'username and postId required'});
    } else {
        mongoose.connect (config.host, config.db);  
        // retrieve session username from the token
        var token = req.body.token || req.query.token || req.headers['x-access-token'];
        if (token) {
            tokenProperty.token.verify (token, tokenProperty.secret, function (err, decode) {
                if (err) {
                    res.send ({status: 'error', message: 'Error decoding the token'});
                } else {
                    console.log (decode);
                    res.send ({status:'success', message:'decoded the message'});
                }
            });
        } else {
            res.send ({status: 'error', message: 'Error authneticating the access token'});
        }
    }

    mongoose.disconnect ();
    //res.end ();
});

// get all the posts sorted in time order
router.get ('/post/all', function (req, res, next) {

});

module.exports = router;