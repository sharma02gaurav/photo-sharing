/*
    the routing file for user
*/
var express = require ('express');
var router = express.Router();
var assert = require ('assert');
var mongoose = require ('mongoose');
var config = require ('../config/connection_config.js');
var UserModel = require ('../model/user_model.js');
var AuthModel = require ('../model/auth_model.js');
var tokenProperty = require ('../config/token.js');

/**
 *  get a user
*/
router.get ('/user/:id', function (req, res, next) {
    mongoose.connect (config.host, config.db);
    res.type ('json');
    UserModel.find ({username: req.params.id}, function (err, data) {
        assert.equal (null, err);
        if (data == null) data = [{status:"not found"}];
        res.send (data[0]);
        next();
    });
    mongoose.disconnect ();
});

/**
 *  request to create a new user
*/
router.post ('/user', function (req, res) {
    mongoose.connect (config.host, config.db);
    res.type ('json');
    var query = {username: req.body.username};
    
    var promise = UserModel.findOne (query);
    promise.then (function (document) {
        if (document == null) {
            var saveDocument = new UserModel;
            saveDocument.username = req.body.username;
            saveDocument.name = req.body.name;
            saveDocument.password = req.body.password;

            mongoose.disconnect();
            mongoose.connect (config.host, config.db);

            var savePromise = saveDocument.save ();
            savePromise.then (function (newDocument) {
                console.log ('document saved');
                if (newDocument == null) {
                    res.send ({status: 'error', message: 'Server error'});
                }  else {
                    // create auth document
                    var authDocument = new AuthModel;
                    authDocument.username = req.body.username;
                    authDocument.password = req.body.password;

                    mongoose.disconnect();
                    mongoose.connect (config.host, config.db);

                    var authSavePromise = authDocument.save ();
                    authSavePromise.then (function (authSaveDocument) {
                        if (authSaveDocument == null) {
                            res.send ({status: 'error', message: 'Server error'});
                        } else {
                            res.send ({status: 'success', message: 'Created user'});
                        }
                    });

                    mongoose.disconnect();
                }
            });
        } else {
            res.send ({status:'error', message: 'User already exists'});
        }
        mongoose.disconnect();
    });
    mongoose.disconnect ();
});

/**
 * request to update a user
 * first try to find the user and then update
 * only updation of password is supported yet
 */
router.put ('/user', function (req, res) {
    mongoose.connect(config.host, config.db);
    var query = {username: req.body.username};
    UserModel.findOne (query, function (err, data) {
        assert.equal (err);
        if (data == null)
            res.send ({status: 'success', message: 'username not found'});
        else {
            var token = req.body.token || req.query.token || req.headers['x-access-token'];
            if (token) {
                tokenProperty.token.verify (token, tokenProperty.secret, function (err, decoded) {
                    if (err) {
                        res.send ({status: "failure", message: 'Token authnetication error'});
                    } else {
                        var updateQuery = {};
                        if (req.body.password == null)
                            res.send ({status: 'falure', message: 'Require password to update'});
                        else {
                            updateQuery = {"$set": {password: req.body.password}};
                            
                            mongoose.disconnect();
                            mongoose.connect (config.host, config.db);
                            UserModel.update (query, updateQuery, {upsert: true},  function (err, raw) {
                                if (err) {
                                    res.send ({status: 'error', message: 'error'});
                                } else {
                                    mongoose.disconnect();
                                    mongoose.connect (config.host, config.db);
                                    // connect with the AuthSchema and update password there as well
                                    AuthModel.update (query, updateQuery, {upsert: true}, function (err, raw) {
                                        if (err) {
                                             res.send ({status: 'error', message: 'error'});
                                        }else {
                                            res.send ({status: 'success', message: 'password updated'});
                                        }
                                    });
                                    mongoose.disconnect();
                                }
                            });
                            mongoose.disconnect();
                        }
                    }
                });
            }
        }
    });
    mongoose.disconnect();
});

/**
 * delete the user
 */
router.delete ('/user', function (req, res) {
    mongoose.connect (config.host, config.db);
    var query = {username: req.body.username};

    UserModel.findOne (query, function (err, user) {
        if (err) {
            res.send ({status: 'error', message: 'Server error'});
        } else {
            if (user == null) {
                res.send ({status: 'error', message: 'User not found'});
            } else {
                user.remove (function (err, rawData) {
                    if (err) {
                        res.send ({status: 'error', message: 'Error removing User'});
                    } else {
                        // disconnect from this model
                        mongoose.disconnect ();
                        mongoose.connect (config.host, config.db);

                        AuthModel.findOne (query, function (err, user) {
                            if (!err) {
                                user.remove (function (err, rawData) {
                                    if (!err) {
                                        res.send ({status: 'success', message: 'Sucessfully deleted user'});
                                    } else {
                                        res.send ({status: 'error', message: 'Error deleting'});
                                    }
                                });
                            } else {
                                res.send ({status: 'error', message: 'Server error deleting authentication'});
                            }
                        });
                    }
                });
            }
        }
    });
});

module.exports = router;
