/*
*   authentication based routing
*/

var express = require ('express');
var app = express ();
var router = express.Router();
var assert = require ('assert');
var mongoose = require ('mongoose');
var config = require ('../config/connection_config.js');

var AuthModel = require ('../model/auth_model.js');

var tokenProperty = require ('../config/token.js');

app.set ('secret', config.secret);

router.post ('/auth', function (req, res, next) {
    mongoose.connect (config.host, config.db);
    res.type ('json');
    AuthModel.findOne ({username: req.body.username, password: req.body.password}, function (err, data) {
        assert.equal (null, err);
        mongoose.disconnect ();
        if (data == null) {
            res.send ({status: "error", message: "username or password error"});
            return;
        } else {
            var token = tokenProperty.token.sign (data, app.get ('secret'));
            res.send ({status:'success', message: 'authenticated', token: token});
            /*also set the session here*/
        }
        /*if (data == null) data = {status: "error", message: "username or password error"};
        res.send (data);*/
    });
});

module.exports = router;
