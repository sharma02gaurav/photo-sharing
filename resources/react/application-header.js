var ApplicationHeader = React.createClass ({
    render: function () {
        return <section className='header container-fluid'>
            <img src='/res/img/photo.png' height='30' alt='logo'/>
            <section className='user-menu pull-right'>
                    <input type='text'  className='search-box' placeholder='Search'/>
                    <img src='/res/img/profile.png' className='img-circle' height='30' alt='photo'/>
            </section>
        </section>;
    }
});

ReactDOM.render (<ApplicationHeader />, document.getElementById ('#application-header'));
