var PicturePostModal = React.createClass ({
    render: function (){
        return <div className="modal fade" id='newImagePostModal' tabindex="-1" role="dialog">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 className="modal-title">Write a new Picture Post</h4>
              </div>
              <div className="modal-body">
                  <textarea className='write-feed-area' placeholder="What you are upto?"></textarea><br/>
                  <button className='btn btn-default btn-sm'><i className='fa fa-camera'></i></button>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="button" className="btn btn-success">Post</button>
              </div>
            </div>
          </div>
      </div>;
    }
});

var PostModal = React.createClass ({
    render: function () {
        <div className="modal fade" id='newPostModal' tabindex="-1" role="dialog">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 className="modal-title">Write a new Post</h4>
              </div>
              <div className="modal-body">
                  <textarea className='write-feed-area' placeholder="What you are upto?"></textarea><br/>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="button" className="btn btn-success">Post</button>
              </div>
            </div>
          </div>
      </div>;
    }
});

ReactDOM.render (<PicturePostModal/>, document.getElementById ('pic-post-modal'));
ReactDOM.render (<PostModal/>, document.getElementById ('post-modal'));
