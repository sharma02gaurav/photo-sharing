/*
    the javascript file for account page
*/

var accountModule = angular.module ('accountModule', []);
/*
    controller for loading posts
*/
accountModule.controller ('AccountPostsController', ['$scope', '$http', function ($scope, $http) {
    $scope.posts = [];

    $scope.loadFeeds = function () {
        // feeds loading code here
        console.log ('created the module');
    };

    $scope.createPost = function () {
        console.log ('request to create a post');
    }
}]);

var showPhotoPostModal = function () {
    $('#newImagePostModal').modal ('toggle');
};

var showPostModal = function () {
    console.log ('calling the show post function ');
    $('#newPostModal').modal ('toggle');
};
